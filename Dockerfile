FROM python:3.9

RUN apt-get install libpq-dev

RUN mkdir /app
ADD ./app/requirements.txt /app/


RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r /app/requirements.txt

WORKDIR /app
